﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GkinisPetros
{
    public static class XLSXPrinting
    {
        public static void DoYourThing(IEnumerable<DedomenaFarmakou> listToPrint,string mode = "")
        {
            var printList = listToPrint.ToList();
            string directory = @"C:\DedomenaEtiketon\", filenamePelates = @"tempCoupons", suffix = @".xlsx";

            FileInfo newFile = new FileInfo(directory + filenamePelates + suffix);

            try { Directory.CreateDirectory(directory); }
            catch (Exception e)
            {
                if (e is DirectoryNotFoundException || e is UnauthorizedAccessException)
                    MessageBox.Show("Λείπει ο φάκελος: " + directory);
                else
                    MessageBox.Show("Το όνομα φακέλου: " + directory + " δεν είναι έγκυρο.");
                MessageBox.Show("Τα αρχεία θα δημιουργηθούν στο φάκελο του προγράμματος.\nΓια να μην επαναληφθεί αυτό το μήνυμα, δημιουργήστε το φάκελο " + directory);

                directory = "";
            }

            if (newFile.Exists)
            {
                try
                {
                    File.Delete(directory+filenamePelates+suffix);
/*                    for (int i = 1; ; i++)
                    {
                        newFile = new FileInfo(directory + filenamePelates + "(" + i + ")" + suffix);
                        if (newFile.Exists == false)
                            break;
                        if (i > 5000)
                        {
                            MessageBox.Show("Έχετε υπερβολικά πολλά αντίγραφα, καθαρίστε το: " + directory);
                            return;
                        }
                    }*/
                }
                catch (System.AccessViolationException)
                {
                    MessageBox.Show("Δεν έχετε δικαίωμα εγραφής στο αρχείο ή το αρχείο είναι ήδη ανοιχτό");
                    return;
                }
            }

            using (ExcelPackage package = new ExcelPackage(newFile))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Φύλλο1");
                //Add the headers
                worksheet.Cells[1, 1].Value = "lian";
                worksheet.Cells[1, 2].Value = "concatenated";
                worksheet.Cells[1, 3].Value = "Unique";
                worksheet.Cells[1, 4].Value = "barcode";

                int j = 1;
                foreach (var kouponi in printList)
                {
                    j++;
                    worksheet.Cells[j, 1].Value = kouponi.Price.ToString("#.00") + mode;
                    worksheet.Cells[j, 2].Value = kouponi.Description + " " + kouponi.Morfi;
                    worksheet.Cells[j, 3].Value = kouponi.UniqueBarcode;
                    worksheet.Cells[j, 4].Value = kouponi.Barcode;
                    worksheet.Cells[j, 1].Style.Numberformat.Format = "@";//see number as text
                    worksheet.Cells[j, 2].Style.Numberformat.Format = "@";
                    worksheet.Cells[j, 3].Style.Numberformat.Format = "@";
                    worksheet.Cells[j, 4].Style.Numberformat.Format = "@";
                }
                worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells

                package.Save();
            }
        }
    }
}
