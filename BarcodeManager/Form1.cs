﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace GkinisPetros
{
    public partial class Form1 : Form
    {
        public const int properBarcodeLength = 13;
        public const int maxUniqueBarcodeLength = 12;
        public const int minUniqueBarcodeLength = 12;
        public string MainSaveFile = "data.bin";
        public string MainPrintedSaveFile = "printed.bin";
        public string BackupFile1 = "~data.bin";
        public string BackupFile2 = "~data2.bin";
        const int tooltipTimer = 4000;
        private FarnetDataContext FarnetDataContext1 = new FarnetDataContext();

        bool barcodeFound;

        public Form1()
        {
            InitializeComponent();
            productList = new List<DedomenaFarmakou>();
            printedList = new List<DedomenaFarmakou>();
            if (Properties.Settings.Default.DBConnString != "")
                FarnetDataContext1 = new FarnetDataContext(Properties.Settings.Default.DBConnString);
            this.Size = Properties.Settings.Default.WindowSize;
            this.Location = Properties.Settings.Default.WindowLocation;
            this.WindowState = Properties.Settings.Default.WindowState;
        }

        const string notFoundError = "Το Barcode δεν βρέθηκε.";
        const string wrongLengthError = "Το Barcode πρέπει να έχει 12 ψηφία.";
        const string invalidPriceError = "Η τιμή πρέπει μπορεί να περιέχει μόνο αριθμούς.";
        const string invalidBarcodeError = "Το Barcode μπορεί να περιέχει μόνο αριθμούς.";
        const string barcodeDuplicateError = "Το μοναδικό Barcode υπάρχει ήδη.";
        const string invalidDateError = "Εσφαλμένη Ημερομηνία";
        Product tempProduct;
        public List<DedomenaFarmakou> productList;
        public List<DedomenaFarmakou> printedList;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string barcode = barcodeTextBox.Text;
            if (barcode.Length == properBarcodeLength)
            {
                if (IsDigitsOnly(barcode))
                {
                    //check the DB
                    var selectByBarcodeQuery = FarnetDataContext1.Products.Where(x => x.ProductBarcodes.Any(y => y.BRAP_AP_BARCODE == barcodeTextBox.Text));
                    try
                    {
                        tempProduct = selectByBarcodeQuery.FirstOrDefault();
                        productBindingSource.DataSource = selectByBarcodeQuery;
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        MessageBox.Show("Δεν βρέθηκε η βάση δεδομένων.", "Σφάλμα αναζήτησης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    barcodeFound = (tempProduct != null);
                    if (barcodeFound)
                    {
                        priceTextBox.Text = tempProduct.AP_TIMH_LIAN.GetValueOrDefault().ToString("#.##");
                        SelectNextControl(barcodeTextBox, true, true, true, true);
                    }
                    else
                    {
                        barcodeTextBox.SelectAll();
                        barcodeErrorTooltip.Show(notFoundError, barcodeTextBox, 0, 20, tooltipTimer);
                    }
                }
                else
                {
                    barcodeFound = false;
                    barcodeTextBox.SelectAll();
                    barcodeErrorTooltip.Show(invalidBarcodeError, barcodeTextBox, 0, 20, tooltipTimer);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadListFromSaves();
        }

        private void έξοδοςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckTexBoxes())
            {
                barcodeTextBox.Focus();
                AddToSavedList();
                SaveToFile();
                ClearTextBoxes();
            }
        }

        private void ClearTextBoxes()
        {
            barcodeTextBox.Clear();
            uniqueBarcodeTextBox.Clear();
            dateTimePicker1.Value = DateTime.Today;
            priceTextBox.Clear();
            maskedTextBox1.Clear();
        }

        private void AddToSavedList()
        {
            DateTime? date;
            if (maskedTextBox1.TextLength == 10)
            {
                DateTime dateNotNull;
                DateTime.TryParse(maskedTextBox1.Text, out dateNotNull);
                if (dateNotNull == DateTime.MinValue)
                {
                    MessageBox.Show("Η ημερομηνία " + maskedTextBox1.Text + " δεν είναι έγκυρη.\nΘα τεθεί κενή.");
                    date = null;
                }
                else
                    date = dateNotNull;
            }
            else
                date = null;
            productList.Add(new DedomenaFarmakou(tempProduct, barcodeTextBox.Text, uniqueBarcodeTextBox.Text, date, priceTextBox.Text));
            UpdateDataGridViewDataSource(dataGridView2, productList);
        }

        void UpdateDataGridViewDataSource(DataGridView dg, IEnumerable<DedomenaFarmakou> list)
        {
            var source = list.ToList();
            //source.Reverse();
            source.OrderBy(x => x.Description).ThenBy(x => x.Morfi);
            dg.DataSource = new SortableBindingList<DedomenaFarmakou>(source);
        }

        private bool CheckTexBoxes()
        {
            var a = PriceValidity();
            var b = UniqueBarcodeValidity();
            var c = BarcodeValidity();
            return a && b && c;
        }

        bool PriceValidity()
        {
            decimal k;
            if (!IsTextBoxDecimal(priceTextBox, out k))
            {
                priceTextBox.SelectAll();
                priceErrorToolTip.Show(invalidPriceError, priceTextBox, 0, 20, tooltipTimer);
                return false;
            }
            return true;
        }

        bool UniqueBarcodeValidity()
        {
            string errorMessage;
            if (!IsUniqueBarcode(uniqueBarcodeTextBox.Text, barcodeTextBox.Text, out errorMessage))
            {
                uniqueBarcodeTextBox.SelectAll();
                uniqueBarcodeToolTip.Show(errorMessage, uniqueBarcodeTextBox, 0, 20, tooltipTimer);
                return false;
            }
            if (uniqueBarcodeTextBox.Text.Length != maxUniqueBarcodeLength)
            {
                uniqueBarcodeTextBox.SelectAll();
                uniqueBarcodeToolTip.Show(wrongLengthError, uniqueBarcodeTextBox, 0, 20, tooltipTimer);
                return false;
            }
            return true;
        }

        bool BarcodeValidity()
        {
            if (!barcodeFound)
            {
                barcodeTextBox.SelectAll();
                barcodeErrorTooltip.Show(notFoundError, barcodeTextBox, 0, 20, tooltipTimer);
                return false;
            }
            return true;
        }

        private bool IsTextBoxDecimal(TextBox t, out decimal g)
        {
            return decimal.TryParse(t.Text.Replace(".", ","), out g);
        }

        private void αποθήκευσηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveToFile();
        }

        void SaveToFile()
        {
            string mantissa = "temp";
            int i = 0;
            while (true)
            {
                if (File.Exists(MainSaveFile + mantissa))
                {
                    mantissa += i.ToString();
                    i++;
                }
                else
                    break;
            }
            try
            {
                using (var a = File.Create(MainSaveFile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<DedomenaFarmakou>>(a, productList);
                }
                File.Delete(MainSaveFile);
                File.Copy(MainSaveFile + mantissa, MainSaveFile);
                File.Delete(MainSaveFile + mantissa);
            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (DirectoryNotFoundException e)
            {
                MessageBox.Show("Σφάλμα στο όνομα φακέλου του αρχείου δεδομένων.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (UnauthorizedAccessException e)
            {
                MessageBox.Show("Δεν έχετε δικαιώματα ανάγνωσης του αρχείου αποθηκευμένων δεδομένων.\nΔοκιμάστε να αλλάξετε θέση στο φάκελο του προγράμματος, ή να το εκτελέσετε ως διαχειριστής.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IOException e)
            {
                MessageBox.Show("Σφάλμα κατά το άνοιγμα του αρχείου.\nΠροσπαθήστε ξανά αργότερα.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        bool StorePrinted()
        {
            string mantissa = DateTime.Today.ToString("_dd-MM-yyyy");
            int i = 0;
            while (true)
            {
                if (File.Exists(MainPrintedSaveFile + mantissa))
                {
                    mantissa += "(" + i.ToString() + ")";
                    i++;
                }
                else
                    break;
            }
            try
            {
                using (var a = File.Create(MainPrintedSaveFile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<DedomenaFarmakou>>(a, productList);
                    return true;
                }
            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (DirectoryNotFoundException e)
            {
                MessageBox.Show("Σφάλμα στο όνομα φακέλου του αρχείου δεδομένων.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (UnauthorizedAccessException e)
            {
                MessageBox.Show("Δεν έχετε δικαιώματα ανάγνωσης του αρχείου αποθηκευμένων δεδομένων.\nΔοκιμάστε να αλλάξετε θέση στο φάκελο του προγράμματος, ή να το εκτελέσετε ως διαχειριστής.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (IOException e)
            {
                MessageBox.Show("Σφάλμα κατά το άνοιγμα του αρχείου.\nΠροσπαθήστε ξανά αργότερα.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return false;
        }

        private void φόρτωσηΔεδομένωνToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadListFromSaves();
        }

        private void LoadListFromSaves()
        {
            string saveFile;
            if (File.Exists(MainSaveFile))
                saveFile = MainSaveFile;
            else
            {/*
                if (File.Exists(BackupFile1))
                    saveFile = BackupFile1;
                else if (File.Exists(BackupFile2))
                    saveFile = BackupFile2;
                else
                {*/
                //MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
                // }
            }
            try
            {
                using (var file = File.OpenRead(saveFile))
                {
                    productList = ProtoBuf.Serializer.Deserialize<List<DedomenaFarmakou>>(file);
                    file.Close();
                    UpdateDataGridViewDataSource(dataGridView2, productList);
                }

            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (DirectoryNotFoundException e)
            {
                MessageBox.Show("Σφάλμα στο όνομα φακέλου του αρχείου δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (UnauthorizedAccessException e)
            {
                MessageBox.Show("Δεν έχετε δικαιώματα ανάγνωσης του αρχείου αποθηκευμένων δεδομένων.\nΔοκιμάστε να αλλάξετε θέση στο φάκελο του προγράμματος, ή να το εκτελέσετε ως διαχειριστής.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IOException e)
            {
                MessageBox.Show("Σφάλμα κατά το άνοιγμα του αρχείου.\nΠροσπαθήστε ξανά αργότερα.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void εκκίνησηΕκτύπωσηςToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            XLSXPrinting.DoYourThing(productList);
            if (StorePrinted())
            {
                productList.Clear();
                UpdateDataGridViewDataSource(dataGridView2, productList);
                SaveToFile();
            }
            else
            {
                if (MessageBox.Show("Παρουσιάστηκε σφάλμα κατά την αποθήκευση των εκτυπωμένων κουπονιών.\nΕάν θέλετε να ΧΑΘΟΥΝ τα εκτυπωμένα κουπόνια πατήστε άκυρο.\nΑλλιώς πατήστε ξανά κάποιο απ' τα κουμπιά εκτύπωσης", "Σφάλμα κατά την αποθήκευση εκτυπωμένων", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
                {
                    MessageBox.Show("Τα κουπόνια δεν αποθηκεύτηκαν.");
                    productList.Clear();
                    UpdateDataGridViewDataSource(dataGridView2, productList);
                }
            }
            string labelFilepath = @"couponsAutoOLD.lbl";
            FileInfo labelFile = new FileInfo(labelFilepath);
            if (labelFile.Exists)
            {
                try
                {
                    System.Diagnostics.Process.Start(labelFilepath);
                }
                catch (System.AccessViolationException)
                {
                    MessageBox.Show("Δεν έχετε δικαιώματα για να ανοίξετε το αρχείο ετικετών: " + labelFilepath + ". Προσπαθήστε να εκτελέσετε το πρόγραμμα ως διαχειριστής, ή να αλλάξετε τα δικαιώματα του αρχείου.", "Σφάλμα Εκκίνησης Εκτύπωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Tο αρχείο ετικετών : \"" + labelFilepath + "\" δεν βρέθηκε.", "Σφάλμα Εκκίνησης Εκτύπωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void FindExpired(DateTime comparison)
        {
            comparison = comparison.Date;
            if (productList == null)
                return;
            var q = productList.Where(x => x.ExpirationDate <= comparison).ToList();
            UpdateDataGridViewDataSource(dataGridView2, q);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //FindExpired(dateTimePicker2.Value);
            DateTime date = new DateTime();
            if (DateTime.TryParse(maskedTextBox2.Text, out date))
            {
                FindExpired(date);
            }
            else
            {
                MessageBox.Show("Η ημερομηνία δεν είναι έγκυρη (ΗΗ/ΜΜ/ΧΧΧΧ)");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var today = DateTime.Today;
            int month = today.Month;
            int year = today.Year;
            int lastDayOfMonth = DateTime.DaysInMonth(year, month);
            var endOfMonth = new DateTime(year, month, lastDayOfMonth);
            FindExpired(endOfMonth);
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if ((senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn) || (senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn) && e.RowIndex >= 0)
            {
                productList.Remove(((DedomenaFarmakou)senderGrid.Rows[e.RowIndex].DataBoundItem));
                senderGrid.Rows.RemoveAt(e.RowIndex);
                SaveToFile();
            }
        }

        public bool IsUniqueBarcode(string uniqueBarcode, string barcode, out string errorMessage)
        {
            if (uniqueBarcode != string.Empty && !IsDigitsOnly(uniqueBarcode))
            {
                errorMessage = invalidBarcodeError;
                return false;
            }
            if (productList != null && productList.Any(x => x.Barcode == barcode && x.UniqueBarcode == uniqueBarcode))
            {
                errorMessage = barcodeDuplicateError;
                return false;
            }
            errorMessage = "";
            return true;
        }
        bool IsDigitsOnly(string str)

        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        private void showProdListButton_Click(object sender, EventArgs e)
        {
            UpdateDataGridViewDataSource(dataGridView2, productList);
        }

        private void LoadPrinted()
        {
            DirectoryInfo taskDirectory = new DirectoryInfo(Environment.CurrentDirectory);
            FileInfo[] taskFiles = taskDirectory.GetFiles(MainPrintedSaveFile + "*");
            if (printedList == null)
                printedList = new List<DedomenaFarmakou>();
            else
                printedList.Clear();
            foreach (FileInfo fi in taskFiles)
            {
                try
                {
                    using (var file = fi.OpenRead())
                    {
                        printedList.AddRange(ProtoBuf.Serializer.Deserialize<List<DedomenaFarmakou>>(file));
                        file.Close();
                    }
                }
                catch (ArgumentException e)
                {
                    MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (DirectoryNotFoundException e)
                {
                    MessageBox.Show("Σφάλμα στο όνομα φακέλου του αρχείου δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (UnauthorizedAccessException e)
                {
                    MessageBox.Show("Δεν έχετε δικαιώματα ανάγνωσης του αρχείου αποθηκευμένων δεδομένων.\nΔοκιμάστε να αλλάξετε θέση στο φάκελο του προγράμματος, ή να το εκτελέσετε ως διαχειριστής.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (IOException e)
                {
                    MessageBox.Show("Σφάλμα κατά το άνοιγμα του αρχείου.\nΠροσπαθήστε ξανά αργότερα.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void priceTextBox_Leave(object sender, EventArgs e)
        {
            priceTextBox.Text = priceTextBox.Text.Replace(".", ",");
        }

        private SortOrder getSortOrder(int columnIndex)
        {
            var direction = dataGridView2.Columns[columnIndex].HeaderCell.SortGlyphDirection;
            if (direction == SortOrder.None ||
                direction == SortOrder.Descending)
            {
                direction = SortOrder.Ascending;
                return SortOrder.Ascending;
            }
            else
            {
                direction = SortOrder.Descending;
                return SortOrder.Descending;
            }
        }

        private void αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectDB n = new SelectDB();
            DialogResult res = n.ShowDialog();
            //string s = System.Configuration.ConfigurationManager.ConnectionStrings["portalConnectionString-Prod"].ConnectionString;
            if (res == DialogResult.OK)
                FarnetDataContext1 = new FarnetDataContext(Properties.Settings.Default.DBConnString);
            //FarnetDataContext1.Connection.ConnectionString = @"Data Source=(local)\CSASQL;Initial Catalog=Farnet_2015;Integrated Security=True";
            //MessageBox.Show(FarnetDataContext1.Connection.ConnectionString);
            /*
            string newLocation = "";
            string defaultDirectory = @"C:\FarmakoNet\SQLData\";
            if (Directory.Exists(defaultDirectory))
                openFileDialog1.InitialDirectory = defaultDirectory;
            var result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
                newLocation = openFileDialog1.FileName;
            if (newLocation == "")
                return;
            else
            {
                Properties.Settings.Default.DBName = newLocation;
                Properties.Settings.Default.Save();
            }*/
        }

        private void dataGridView2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView2.Columns[e.ColumnIndex].Name == "Price")
                sumTextBox.Text = CellSum(e.ColumnIndex).ToString();
        }

        private decimal CellSum(int column)
        {
            decimal sum = 0;
            for (int i = 0; i < dataGridView2.Rows.Count; ++i)
            {
                decimal d = 0;
                if (dataGridView2.Rows[i].Cells[column].Value != null)
                    decimal.TryParse(dataGridView2.Rows[i].Cells[column].Value.ToString(), out d);
                sum += d;
            }
            return sum;
        }

        private void dataGridView2_DataSourceChanged(object sender, EventArgs e)
        {
            if (dataGridView2.DataSource != null && dataGridView2.RowCount > 0)
            {
                var a = dataGridView2.Columns["Price"];
                if (a != null)
                {
                    sumTextBox.Text = CellSum(a.Index).ToString();
                }
            }
            else
                sumTextBox.Text = "0";
        }

        private void sumTextBox_TextChanged(object sender, EventArgs e)
        {
            if (sumTextBox.Text.Length > 0)
            {
                if (sumTextBox.Text.Last() != '\u20AC')
                    sumTextBox.Text += " \u20AC";
            }
            else
                sumTextBox.Text = "0 \u20AC";
        }

        private void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (maskedTextBox1.Text.Length == 10)
            {
                if (CheckDate(maskedTextBox1.Text))
                    SelectNextControl(maskedTextBox1, true, true, true, true);
                else
                {
                    maskedTextBox1.SelectAll();
                    dateErrorTooltip.Show(invalidDateError, maskedTextBox1, 0, 20, tooltipTimer);
                }
            }

        }

        private bool CheckDate(string DateString)
        {
            DateTime date;
            DateTime.TryParse(DateString, out date);
            if (date == DateTime.MinValue)
                return false;
            else
                return true;
        }

        private void εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            XLSXPrinting.DoYourThing(productList, " €");
            string labelFilepath = @"couponsAutoPLUS.lbl";
            FileInfo labelFile = new FileInfo(labelFilepath);
            if (labelFile.Exists)
            {
                try
                {
                    System.Diagnostics.Process.Start(labelFilepath);
                }
                catch (System.AccessViolationException)
                {
                    MessageBox.Show("Δεν έχετε δικαιώματα για να ανοίξετε το αρχείο ετικετών¨: " + labelFilepath + ". Προσπαθήστε να εκτελέσετε το πρόγραμμα ως διαχειριστής, ή να αλλάξετε τα δικαιώματα του αρχείου.", "Σφάλμα Εκκίνησης Εκτύπωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Tο αρχείο ετικετών : \"" + labelFilepath + "\" δεν βρέθηκε.", "Σφάλμα Εκκίνησης Εκτύπωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (CheckTexBoxes())
                {
                    barcodeTextBox.Focus();
                    AddToSavedList();
                    SaveToFile();
                    ClearTextBoxes();
                }
            }
        }

        private void priceTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (CheckTexBoxes())
                {
                    barcodeTextBox.Focus();
                    AddToSavedList();
                    SaveToFile();
                    ClearTextBoxes();
                }
            }
        }

        private void textBoxDelete_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && textBoxDelete.Text.Length == maxUniqueBarcodeLength && productList != null)
            {
                int productIndex = productList.FindIndex(x => x.UniqueBarcode == textBoxDelete.Text);
                if (productIndex < 0)
                {
                    textBoxDelete.SelectAll();
                    MessageBox.Show(this, "Tο μοναδικό barcode δεν βρίσκεται στη λίστα.", "Διαγραφή Barcode", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    productList.RemoveAt(productIndex);
                    SaveToFile();
                    textBoxDelete.Clear();
                    UpdateDataGridViewDataSource(dataGridView2, productList);
                }
            }
        }

        private void dataGridView2_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            textBoxCount.Text = dataGridView2.RowCount.ToString();
        }

        private void dataGridView2_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            textBoxCount.Text = dataGridView2.RowCount.ToString();
            if (dataGridView2.DataSource != null && dataGridView2.RowCount > 0)
            {
                var a = dataGridView2.Columns["Price"];
                if (a != null)
                {
                    sumTextBox.Text = CellSum(a.Index).ToString();
                }
            }
            else
                sumTextBox.Text = "0";
        }

        private void uniqueBarcodeTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return && uniqueBarcodeTextBox.Text.Length == maxUniqueBarcodeLength)
                SelectNextControl(uniqueBarcodeTextBox, true, true, true, true);
        }

        private void showPrintedList_Click(object sender, EventArgs e)
        {
            LoadPrinted();
            if (printedList != null)
                UpdateDataGridViewDataSource(dataGridView2, printedList);
        }

        private void dataGridView2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 5)
                {
                    DateTime date;
                    if (!DateTime.TryParse(dataGridView2.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(), out date))
                        dataGridView2.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = null;
                }
                SaveToFile();
            }
        }

        private void dataGridView2_Sorted(object sender, EventArgs e)
        {
            if (dataGridView2.DataSource != null && dataGridView2.RowCount > 0)
            {
                var a = dataGridView2.Columns["Price"];
                if (a != null)
                {
                    sumTextBox.Text = CellSum(a.Index).ToString();
                }
            }
            else
                sumTextBox.Text = "0";
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.WindowState = this.WindowState;
            if (this.WindowState == FormWindowState.Normal)
                Properties.Settings.Default.WindowSize = this.Size;
            else
                Properties.Settings.Default.WindowSize = this.RestoreBounds.Size;
            Properties.Settings.Default.WindowLocation = this.Location;
            Properties.Settings.Default.Save();
        }

        private void εκτύπωσηΤρέχουσαςΛίσταςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sum = productList.Sum(x => x.Price);
            var datagridviewList = ((SortableBindingList<DedomenaFarmakou>)dataGridView2.DataSource).ToList();
            PrintingManager p = new PrintingManager(sum, sum, sum, datagridviewList);
            p.PrintAll();
        }
    }
}