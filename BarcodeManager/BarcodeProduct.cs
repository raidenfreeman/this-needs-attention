﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GkinisPetros
{
    [ProtoBuf.ProtoContract]
    public class DedomenaFarmakou
    {
        //public Product productReference { get; set; }
        [ProtoBuf.ProtoMember(1)]
        public string Description { get; set; }
        [ProtoBuf.ProtoMember(2)]
        public string Morfi { get; set; }
        [ProtoBuf.ProtoMember(3)]
        public string Barcode { get; set; }
        [ProtoBuf.ProtoMember(4)]
        public string UniqueBarcode { get; set; }

        [ProtoBuf.ProtoMember(5)]
        public decimal Price { get; set; }
        [ProtoBuf.ProtoMember(6)]
        public DateTime? ExpirationDate { get; set; }
        //[ProtoBuf.ProtoMember(7)]
        //public DateTime PurchaseDate { get; set; }
        //[ProtoBuf.ProtoMember(7)]
        //public Customer Customer { get; set; }

        private DedomenaFarmakou()
        {

        }

        public DedomenaFarmakou(Product product, string barcode, string uniqueBarcode, DateTime? date, string price)
        {
            Description = product.AP_DESCRIPTION;
            Morfi = product.AP_MORFI;
            Barcode = barcode;
            UniqueBarcode = uniqueBarcode;
            if (price == null)
                Price = product.AP_TIMH_LIAN.GetValueOrDefault();
            else
                Price = decimal.Parse(price);
            ExpirationDate = date;
        }
    }
}
