﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GkinisPetros
{
    public partial class SelectDB : Form
    {
        public SelectDB()
        {
            InitializeComponent();
        }


        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Properties.Settings.Default.DBConnString = @"Data Source = (local)\CSASQL; Initial Catalog = Farnet_" + textBox1.Text + "; Integrated Security = True";
                Properties.Settings.Default.Save();
                MessageBox.Show("Η βάση άλλαξε στην: " + "Farnet_" + textBox1.Text);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}

/*
private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
{
    if (e.KeyChar == (char)ConsoleKey.Enter && textBox1.Text.Length > 0)
    {
        int year = 0;
        int.TryParse(textBox1.Text, out year);
        if (year < 1900 && year > 2200)
        {
            MessageBox.Show("Λάθος έτος. Πρέπει να είναι μεταξύ 1900 και 2200.");
            textBox1.SelectAll();
        }
        else
        {
            string tempConnString = @"Data Source = (local)\CSASQL; Initial Catalog = Farnet_" + year.ToString() + "; Integrated Security = True";
            try
            {
                FarnetDataContext dc1 = new FarnetDataContext(tempConnString);
                var a = dc1.Products.First();
            }
            catch
            {
                textBox1.SelectAll();
                MessageBox.Show("Η βάση Farnet_" + year + " δεν βρέθηκε.");
                return;
            }
            Properties.Settings.Default.DBConnString = tempConnString;
            Properties.Settings.Default.Save();
            this.Close();
        }

    }*/
