﻿using UnityEngine;
using System.Collections;

public class BulletMovement : MonoBehaviour
{

    public int speed = 50;

    public int boundaryTop = 174;

    public int boundaryBottom = -200;

    void Update()
    {
        transform.Translate(0, Time.deltaTime * speed, 0);
        if (transform.position.y > boundaryTop || transform.position.y < boundaryBottom)
            this.gameObject.SetActive(false);
    }
    void OnEnable()
    {
        GetComponent<AudioSource>().Play();
    }
}
