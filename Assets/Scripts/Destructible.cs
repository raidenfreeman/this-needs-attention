﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Destructible : MonoBehaviour {

    public AudioSource audioManager;
    public AudioClip destructionSound;
    public int pointValue;
    public static int score;
    public Text scoreText;
    public bool isBonus;
    public FleetManager fleetManager;

    public AnimationData animationData;

	void Start () {
        if (transform.parent != null)
            fleetManager = transform.parent.GetComponent<FleetManager>();
        //if (audioManager == null || destructionSound == null)
            //Debug.Log("Missing References!");
	}
	
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (!coll.CompareTag(this.tag))
        {
            IncreaseScore();
            BecomeInactive();
            coll.gameObject.SetActive(false); //Deactivate the object that collided with you
        }
    }

    void IncreaseScore()
    {
        score += pointValue;
        if (isBonus)
            score += 50 * Random.Range(0, 4);
        scoreText.text = score.ToString();
    }
    
    void BecomeInactive()
    {
        PlayDestructionAnimation();
        this.gameObject.SetActive(false);
        if (isBonus == false)
            fleetManager.CheckForEmptyFleet();
    }

    void PlayDestructionAnimation()
    {
        audioManager.PlayOneShot(destructionSound);
        if (animationData != null)
            StartCoroutine(animationData.PlayAnimation());
    }

}
