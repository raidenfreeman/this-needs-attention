﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteRenderer))]
public class Shield : MonoBehaviour {
    
    public int hp, maxhp;

    public Sprite[] damageStateSprites;

    private SpriteRenderer spriteRenderer;
    
    void Start()
    {
        hp = maxhp;
        spriteRenderer = this.GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (!coll.CompareTag(this.tag))
        {
            ReceiveDamage(1);
            coll.gameObject.SetActive(false);
        }
    }

    void ReceiveDamage(int damage)
    {
        hp -= damage;
        if (hp <= 0)
            BecomeInactive();
        else
            AdjustSpriteToHP();
    }

    void AdjustSpriteToHP()
    {
        if (maxhp <= 0)
            return;
        spriteRenderer.sprite = damageStateSprites[hp - 1];
    }

    void BecomeInactive()
    {
        this.gameObject.SetActive(false);
    }

}
