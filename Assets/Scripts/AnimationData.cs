﻿using UnityEngine;
using System.Collections;

/*public struct AnimationFrame
{
    Sprite frameSprite;
    float timeOffset;
}*/

public class AnimationData
{

    public SpriteRenderer targetSpriteRenderer;
    public Sprite[] animationFrames;
    public float timePerFrame;
    public float[] timePerFrameOffset;
    //public AnimationFrame[] animationFrames;
    public int numberOfFrames { get; private set; }

    public AnimationData(Sprite[] animationFrames, float timePerFrame, float[] timePerFrameOffset, SpriteRenderer targetRenderer)
    {
        this.targetSpriteRenderer = targetRenderer;
        if (animationFrames == null)
            this.animationFrames = new Sprite[] { targetRenderer.sprite };
        else
            this.animationFrames = animationFrames;
        this.timePerFrame = timePerFrame;
        numberOfFrames = animationFrames.Length;
        if (timePerFrameOffset == null)
            this.timePerFrameOffset = new float[numberOfFrames];
        else
            this.timePerFrameOffset = timePerFrameOffset;

        //if we were given less time offsets than sprites, add zeroes to the offsets
        if (this.timePerFrameOffset.Length < this.animationFrames.Length)
        {
            this.timePerFrameOffset = new float[numberOfFrames];
            int i = 0;
            foreach (float f in timePerFrameOffset)
            {
                this.timePerFrameOffset[i] = f;
                i++;
            }
        }
    }

    public IEnumerator PlayAnimation()
    {
        if (numberOfFrames > 1)
        {
            int frame = 0;
            foreach (Sprite s in animationFrames)
            {
                targetSpriteRenderer.sprite = s;
                yield return new WaitForSeconds(timePerFrame + timePerFrameOffset[frame]);
                frame++;
            }
        }
    }
}
